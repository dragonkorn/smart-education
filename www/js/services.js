angular.module('starter.services', [])
.factory('dataBase', function() {
  

  var studentUser = [
    {
      userId : '000001',
      //profile of student
      profile: {
        name    : 'Rukdee',
        lastname: 'Konkeng',
        role    : 'student',
        student_code : '14147',
        sex     : 'male',
        //room    : '',
        e_mail  : 'rukdee1234@gmail.com',
        date_of_birth: '1/1/2005',
        address : '123/456 Ladkrabang Ladkrabang Bangkok 10520',
        tel     : '081-234-5678',
        bloodType: 'A'
      },
      
      schedule:{
        
      },
      
      gradeBook:{
        GPA         :'3.27',
        semester:[
          {
            semesterId:0,
            title   :'1/2558',
            GPS     : '3.44', //credit = 17
            subjects :[
              {
                title   :'ELECTRONICS FOR COMPUTER ENGINEERING',
                credit  :'3',
                grade   :'B+'
              },{
                title   :'CIRCUITS AND ELECTRONICS LABORATORY',
                credit  :'1',
                grade   :'B+'
              },{
                title   :'DIGITAL CIRCUIT AND LOGIC DESIGN',
                credit  :'3',
                grade   :'B+'
              },{
                title   :'DIGITAL CIRCUIT LABORATORY',
                credit  :'1',
                grade   :'A'
              },{
                title   :'DATA STRUCTURES AND ALGORITHMS',
                credit  :'3',
                grade   :'A'
              },{
                title   :'ENGLISH FOR FURTHER STUDIES',
                credit  :'3',
                grade   :'B'
              },{
                title   :'LOVING FAMILY',
                credit  :'3',
                grade   :'B'
            }]
        },{
            semesterId:1,
            title   :'2/2558',
            GPS     :'3.13',   //credit = 19
            subjects :[
              {
                title   :'ADVANCED DIGITAL SYSTEM DESIGN',
                credit  :'3',
                grade   :'C+'
              },{
                title   :'COMPUTER ORGANIZATION',
                credit  :'3',
                grade   :'C+'
              },{
                title   :'COMPUTER INTERFACING',
                credit  :'3',
                grade   :'A'
              },{
                title   :'DATA COMMUNICATIONS',
                credit  :'3',
                grade   :'B+'
              },{
                title   :'DATA COMMUNICATIONS LABORATORY',
                credit  :'1',
                grade   :'A'
              },{
                title   :'PROBABILITY AND STATISTICS',
                credit  :'3',
                grade   :'C+'
              },{
                title   :'ENGLISH FOR DEVELOPING READING SKILLS',
                credit  :'3',
                grade   :'B+'
            }]
        }]
      }
    },{
      userId : '000002',
      //profile of student
      profile: {
        name    : 'Rukrian',
        lastname: 'Konkeng',
        role    : 'student',
        sex     : 'male',
        //room    : '',
        e_mail  : 'rukrian1234@gmail.com',
        date_of_birth: '1/1/2005',
        address : '123/456 Ladkrabang Ladkrabang Bangkok 10520',
        tel     : '081-234-5678',
        bloodType: 'A'
      },
      
      schedule:{

      },
      
      gradeBook:{

      }

    }
  ];

  var contentDay = [
    {
      title : 'may',
      Day : [
      {
        Date_number : '1',
        Activity : [
          {
            Acname : 'Registration',
            Detail : 'Student can come for register themselves',
            Time : '08:00-16:00'
          }
        ]
      },
      {
        Date_number : '2',
      },      
      {
        Date_number : '3',
      },
      {
        Date_number : '4',
      },
      {
        Date_number : '5',
        Activity : [
          {
            Acname : 'Open Term',
            Detail : 'Start Class at 08:00',
            Time : '08:00-16:00'
          },
          {
            Acname : 'Celebrate Teacher',
            Detail : 'Student can come to pay respect to teacher',
            Time : 'All Day'
          }
        ]
      },
      {
        Date_number : '6',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '7',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '8',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '9',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '10',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '11',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '12',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '13',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '14',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '15',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '16',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '17',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '18',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '19',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '20',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '21',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '22',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '23',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '24',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '25',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '26',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '27',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '28',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '29',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '30',
        Activity : '',
        Detail : ''
      },
      {
        Date_number : '31',
        Activity : '',
        Detail : ''
      }
      ]
    }
  ];
  
  var parentUser = [
    {
      userId : '100001',
      profile: {
        name    : '',
        lastname: '',
        e_mail  : 'tel' 
      },
      childen : [
        {childId : '000001'},
        {childId : '000002'}
      ]
    }
  ];



  //subject {math, sci, thai, eng, gym, art,}
  //notification {homework 1, news 2, activities 3, messege 4}
  var notification = [
  {
    type  : 1,
    subject : "math",
    title : "Math",
    date : "20/09/2016",
    detail_title : "Foundation",
    detail : "Please read the book page 20-25 and write the summary in A4 paper.!!"
  },{
    type : 1,
    subject : "sci",
    title : "Science",
    date : "21/09/2016",
    detail_title : "life and environment",
    detail : "Lab 1 seach life around you in home and record"
  },{
    type : 2,
    title : "Scholarship Hor Kan Ka university",
    date : "8/9/2016",
    detail_title : "Guidance",
    detail : "for information please enter this website http://science.utcc.ac.th/"
  },{ 
    type  : 2,
    title : "KMITL quota ",
    date : "19/9/2016",
    detail_title : "Guidance",
    detail : "Engineering , Faculty of Engineering of KMITL Have quota for grade6 student to entrance at KMITL "
  },{
    type  : 2,
    title : "BEAM Camp KMITL",
    date : "22/10/2016",
    detail_title : "Guidance",
    detail : "Biomedical Engineering Alternative Movement Camp send application in 29-9-2016"
  },{
    type  : 3,
    title : "Field trips for Grade 1",
    date : "27/9/2016",
    detail_title : "personnel department",
    detail : "Field trips to Arun temple"
  },{
    type  : 3,
    title : "FM13-9",
    date : "30/9/2016",
    detail_title : "Registration",
    detail : "Teacher must send FM13-9 to Registration"
  },{
    type  : 3,
    title : "Exam Results",
    date : "3/10/2016",
    detail_title : "Registration",
    detail : "Announcement the midterm examination results of 1/2016" 
  }]


  return{
    getstudent: function(index) {
      return studentUser[index];
    },

    getparent: function() {
      return parentUser[0];
    },

    getnoti: function(index){
      return notification[index-1];
    },
    getcontent: function(){
      return contentDay;
    },
    getMonth: function(month){
      var index;
      for(index = 0;index<contentDay.length;index++){
        if(contentDay[index].title === month){
          //console.log(contentDay[index]);
          return index;
        }
      }
    },
    //subject : subject or noti
    //type    : what subject or what type of noti
    getnoti: function(type,subject){          
      var ret_noti = new Array();
      var index = 0; //running index
      var temp;

      //console.log(type,subject);

      while(index < notification.length){
        temp = notification[index];
        
        if(type === 0){
          //console.log(temp);
          if(temp.subject === subject){
            ret_noti.push(temp);
          }
        }
        else if(temp.type === type){
          //console.log(temp);
          ret_noti.push(temp);
        }

        index++;
      }


      if(ret_noti){
        return ret_noti;
      }
      else{
        return NULL;
      }
    },
    addnoti: function(obj){
      notification.push(obj);

      return null;
    }

  }
})
