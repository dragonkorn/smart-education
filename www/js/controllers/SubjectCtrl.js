app.controller('SubjectCtrl', function($scope, $location, $ionicModal, dataBase){

	//ionic modal
  // Create and load the Modal
  	var testsubject;
  	testsubject = dataBase.getnoti(0,"math");
  	console.log(testsubject);



	$ionicModal.fromTemplateUrl('feedback.html', function(modal) {
		$scope.feedbackModal = modal
	}, {
		scope: $scope,
		animation: 'slide-in-up'
	});
	//show modal
  	$scope.openFeedback = function() {
    	$scope.feedbackModal.show();
  	};
  	//hide modal
  	$scope.closeFeedback = function(){
  		$scope.feedbackModal.hide();
  	}
	$scope.createTask = function(task) {
	    //$scope.tasks.push({
	    //  title: task.title
	    //});
	    if(task.title && task.detail){
		    $scope.showAlert("Your message has been sent!!");

		    var obj = {
		    		type  : 4,
				    title : "message",
				    date : "27/8/2016",
				    detail_title : task.title,
				    detail : task.detail
				};
			console.log(obj);
			dataBase.addnoti(obj);

		    $scope.feedbackModal.hide();
		    task.title = "";
		    task.detail = "";
	    }
	    else{
			$scope.showAlert("Invalid Title or Detail");
	    }

	};

	   // An alert dialog



	//style for block
	var color = [
		"rgba(0,125,255,0.8)",
		"rgba(0,0,255,0.7)",
		"rgba(0,125,125,0.8)",
		"rgba(125,125,0,0.8)",
		"rgba(0,150,0,0.8)",
	]


	$scope.block = {
		"background-color" : color[2]
	}


	//subject data
	$scope.subjects = [
		{
			subjectId   : "math",
			title: "Mathematics 6",
			style: {"background-color" : color[0]}
			
		},{
			subjectId   : "sci",
			title: "Science 6",
			style: {"background-color" : color[1]}
		},{
			subjectId   : "eng",
			title: "English 6",
			style: {"background-color" : color[2]}
		},{
			subjectId   : "phy",
			title: "Physical Education 6",
			style: {"background-color" : color[3]}
		},{
			subjectId   : "art",
			title: "Arts 6",
			style: {"background-color" : color[4]}
		}]


	// go to main
	$scope.gotoMain = function(){
		$location.path('/app/main');
	}


	$scope.gotoNoti = function(subject){
		
		var des = "app/noti"+0+subject.subjectId;
		//console.log(des);
		$location.path(des);
	}

})