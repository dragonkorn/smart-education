app.controller('GradebookCtrl', function($scope,$location,dataBase,$stateParams){

	var index = parseInt($stateParams.semester);

	var user = dataBase.getstudent(0);

	$scope.semester = user.gradeBook.semester[index];
	console.log(typeof(index));

	$scope.gpa = user.gradeBook.GPA;


	$scope.gotoMain = function(){
		$location.path('/app/main');
	}

	$scope.previousSemester = function(){
		if(index > 0){
			console.log('previous');
			$location.path('/app/gradebook'+ (index-1));
		}
		else{
			console.log('min!!');
		}
	}	

	$scope.nextSemester = function(){
		if(index+1 < user.gradeBook.semester.length){
			console.log('next');
			$location.path('/app/gradebook'+ (index+1));
		}
		else{
			console.log('max!!');
		}
	}
})