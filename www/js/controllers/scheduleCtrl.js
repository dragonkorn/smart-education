app.controller('scheduleCtrl',function($scope,$location,$state,$ionicSlideBoxDelegate){

$scope.openCity = function(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}

$scope.home = function(){
	$location.path('app/main');
}

//sliding tab
$scope.startApp = function() {
    $state.go('main');
}

$scope.next = function() {
    $ionicSlideBoxDelegate.next();
}
$scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
}

$scope.slideChanged = function(index) {
    $scope.slideIndex = index;
}

$scope.toIntro = function(){
    $state.go('intro');
  }

$scope.check = {
  "background-color" : "red"
}

$scope.a = {"background-color" : "coral"};
$scope.test = function(word){
  alert(word);
}

$scope.switchtoCalendar = function(){
  $location.path('app/calendar/may/first');
}

$scope.schedule =     [
{
      "Day11" : "Math",
      "Day1_1" : {"background-color" : "green"},//{"background-color" : "coral"},  //check("came")
      "Day12" : "Phy",
      "Day1_2" : {"background-color" : "red"},
      "Day13" : "Sci",
      "Day1_3" : {"background-color" : "green"},
      "Day14" : "Thai",
      "Day1_4" : {"background-color" : "red"},
      "Day15" : "Eng",
      "Day1_5" : {"background-color" : "none"}
    },
    {
      "Day21" : "Math",
      "Day2_1" : {"background-color" : "green"},
      "Day22" : "Phy",
      "Day2_2" : {"background-color" : "red"},
      "Day23" : "Sci",
      "Day2_3" : {"background-color" : "green"},
      "Day24" : "Thai",
      "Day2_4" : {"background-color" : "green"},
      "Day25" : "Eng",
      "Day2_5" : {"background-color" : "none"}
    },
    {
      "Day31" : "Math",
      "Day3_1" : {"background-color" : "green"},
      "Day32" : "Phy",
      "Day3_2" : {"background-color" : "red"},
      "Day33" : "Sci",
      "Day3_3" : {"background-color" : "green"},
      "Day34" : "Thai",
      "Day3_4" : {"background-color" : "green"},
      "Day35" : "Eng",
      "Day3_5" : {"background-color" : "none"}
    },
    {
      "Day41" : "Math",
      "Day4_1" : {"background-color" : "red"},
      "Day42" : "Phy",
      "Day4_2" : {"background-color" : "red"},
      "Day43" : "Sci",
      "Day4_3" : {"background-color" : "green"},
      "Day44" : "Thai",
      "Day4_4" : {"background-color" : "green"},
      "Day45" : "Eng",
      "Day4_5" : {"background-color" : "none"}
    },
    {
      "Day51" : "Math",
      "Day5_1" : {"background-color" : "green"},
      "Day52" : "Phy",
      "Day5_2" : {"background-color" : "red"},
      "Day53" : "Sci",
      "Day5_3" : {"background-color" : "green"},
      "Day54" : "Thai",
      "Day5_4" : {"background-color" : "green"},
      "Day55" : "Eng",
      "Day5_5" : {"background-color" : "none"}
    }
    ]

    $scope.dashboard = {swiper: false, slider: false, activeIndexView: 0};

  $scope.$watch('dashboard.slider', function (swiper) {
      if (swiper) {
          $scope.swiper = swiper;

          swiper.on('onSlideChangeStart', function (swiper) {
              if(!$scope.$$phase) {
                  $scope.$apply(function () {
                       $scope.dashboard.activeIndexView = swiper.snapIndex;   
                  }); 
              } else {
                  $scope.dashboard.activeIndexView = swiper.snapIndex;
              }
          });
      }
  });

  $scope.dashboard.slideTo = function (indexSlide) {
    $scope.swiper.slideTo(indexSlide);
  };
});

/**
Copyright 2016 Google Inc. All Rights Reserved. 
Use of this source code is governed by an MIT-style license that can be in foundin the LICENSE file at http://material.angularjs.org/license.
**/