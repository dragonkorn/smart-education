app.controller('NotificationListCtrl',function($scope, $location,$ionicModal, $stateParams, dataBase){


	/* connent to services to get notification data */
	var firstindex = parseInt($stateParams.index[0]);
	var secondindex = $stateParams.index;
	var i = 0;
	while(secondindex.charAt(0) === '0'){
		//console.log(firstindex);
		secondindex = secondindex.substr(1);
		i++;
		if(i === 1000){
			break;
		}
	}

	var noti = dataBase.getnoti(firstindex,secondindex);

	$scope.data = noti;
	/* end of connent to services to get notification data */
	
	//data


	//icon from stateParams.index
	if($stateParams.index[0] === '1' || $stateParams.index[0] === '0'){
		$scope.icon = "ion-ios-home";
		$scope.title = "Homework";
	}
	else if($stateParams.index[0] === '2'){
		$scope.icon = "ion-ios-paper";
		$scope.title = "News";
	}
	else if($stateParams.index[0] === '3'){
		$scope.icon = "ion-speakerphone";
		$scope.title = "Activities";
	}
	else{
		$scope.icon = "ion-android-chat";
		$scope.title = "Messege";
	}
	

	//console.log($scope.data);

	/*ionic modal*/
	$ionicModal.fromTemplateUrl('feedback.html', function(modal) {
		$scope.detailModal = modal
	}, {
		scope: $scope,
		animation: 'slide-in-up'
	});
	$scope.openDetail = function(detail){
		$scope.detail = detail;
		console.log($scope.detail);
		$scope.detailModal.show();
	}
	$scope.closeDetail = function(){
		$scope.detailModal.hide();
	}

	$scope.gotoBack = function(){
		if($stateParams.index[0] === '0'){
			$location.path('app/subjects');
		}
		else{
			$location.path('app/noti');
		}
	}

	
})