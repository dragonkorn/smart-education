// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'starter.controllers' , 'starter.services']);

app.run(function($ionicPlatform , $rootScope, $timeout) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

     $rootScope.authStatus = false;
	 //stateChange event
	  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
		  $rootScope.authStatus = toState.authStatus;
		  if($rootScope.authStatus){
			  
			
		  }
    });

	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		console.log("URL : "+toState.url);
		if(toState.url=='/dashboard'){
			console.log("match : "+toState.url);
			$timeout(function(){
				angular.element(document.querySelector('#leftMenu' )).removeClass("hide");
			},1000);
		}	
	});

})

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

//--------------------------------------

 .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-signin.html'
      }
    },
	authStatus: false
  })

  .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainCtrl'
      }
    }
  })

  .state('app.chooseChild', {
    url: '/parentPage',
    views: {
      'menuContent': {
        templateUrl: 'templates/choose-child.html',
        controller: 'ParentPageCtrl'
      }
    }
  })

  .state('app.profile', {
    url: '/profile',
    views:{
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.gradebook', {
    url: '/gradebook:semester',
    views:{
      'menuContent':{
        templateUrl: 'templates/gradebook.html',
        controller: 'GradebookCtrl'
      }
    }
  })
  
  .state('app.schedule',{
    url: '/schedule',
    views:{
      'menuContent': {
        templateUrl: 'templates/class-schedule.html',
        controller: 'scheduleCtrl'
      }
    }
  })

  .state('app.noti', {
    url: '/noti',
    views:{
      'menuContent': {
        templateUrl: 'templates/notification.html',
        controller: 'NotificationCtrl'
      }
    }
  })

  .state('app.calendar',{
    url: '/calendar',
    views:{
      'menuContent': {
        templateUrl: 'templates/calendar.html',
        controller: 'calendarCtrl'
      }
    }
  })


  .state('app.subjects',{
    url: '/subjects',
    views:{
      'menuContent':{
        templateUrl: 'templates/subjects.html',
        controller: 'SubjectCtrl'
      }
    }
  })

  .state('app.noti-list', {
    url: '/noti:index',
    views:{
      'menuContent': {
        templateUrl: 'templates/notification-list.html',
        controller: 'NotificationListCtrl'

      }
    }
  })

  .state('app.calendar.may',{
    url: '/:month',
    views:{
        'monthCalendar': {
          templateUrl: 'templates/month/may.html',
          controller: 'calendarCtrl'
        }
    }
  })

  .state('app.calendar.may.first',{
    url: '/:day',
    views:{
      'detailCalendar': {
        templateUrl: 'templates/month/first.html'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
