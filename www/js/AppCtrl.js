angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $ionicPopover, $ionicLoading, $timeout,  $location, $ionicPopup, $state) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  //--------------------------------------------
    loading = function() {
        $ionicLoading.show({
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
        });

        // For example's sake, hide the sheet after two seconds
        $timeout(function() {
            $ionicLoading.hide();
        }, 2000);
    };
   $scope.login = function(user) {

		if(typeof(user)=='undefined'){
			$scope.showAlert('Please fill username and password to proceed.');	
			return false;
		}

		if(user.username=='student' && user.password=='1234'){
			loading();
      //$state.go('app.main')
			$location.path('/app/main');
		}else if(user.username=='parent' && user.password=='4321'){
			loading();
			$location.path('/app/parentPage');
		}else{
			$scope.showAlert('warning message','Invalid username or password.');	
		}
		
	};
  //--------------------------------------------
  $scope.logout = function() {   $location.path('/app/login');   };
  //--------------------------------------------
   // An alert dialog
	 $scope.showAlert = function(header,msg) {
	   var alertPopup = $ionicPopup.alert({
		 title: header,
		 template: msg
	   });
	 };
  //--------------------------------------------

  //-------------------temp---------------------
  $scope.profile = function() {
  	$location.path('/app/profile');
  }
});


